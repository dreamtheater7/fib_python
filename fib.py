# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is a fibonacci number program.")

    # recursion
    def fib_recursion(idx):
        if idx < 2:
            return idx
        else:
            return fib_recursion(idx-2)+fib_recursion(idx-1)

    inp_r = 8
    result_r = fib_recursion(inp_r)
    print(f"the fibonacci number of {inp_r} is {result_r}")

    def fib_iteration(idx_i):
        seq = [0,1]
        for i in range(idx_i):
            seq.append(seq[-2]+seq[-1])
        return seq[-2]







if __name__ == "__main__":
    main()

